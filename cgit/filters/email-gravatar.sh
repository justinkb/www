#!/bin/bash
# email-gravatar.sh - Generate URLs to Gravatar images from the emails of committers
#
# Takes an email on the first argument, and text to go after the image on stdin.
#
# Written by Kylie McClain <somasis@exherbo.org>

email=${1}
if [[ "$2" == [0-9]* ]];then
    size=${2}
else
    size=24
fi

if [[ "$3" == [0-9]* ]];then
    scaledsize=${3}
else
    scaledsize=64
fi

#fallback="http%3A%2F%2Fexherbo.org%2Fimg%2Fzebrapig-head.png" # always urlencoded
fallback="https%3A%2F%2Favatars.githubusercontent.com%2F${email%@*}%3Fsize%3D${scaledsize}"

# get rid of [<>], trim and lowercase - https://en.gravatar.com/site/implement/hash/
[[ "${email:0:1}" == '<' ]] && email=${email#<}
[[ "${email: -1}" == '>' ]] && email=${email%>}
email=${email,,}

text=$(</dev/stdin)

md5=$(printf "$email" | md5sum)
md5=${md5// *}

echo "<img src='//www.gravatar.com/avatar/${md5}?s=${scaledsize}&d=${fallback}' width='${size}' height='${size}' class='avatar' alt='Avatar' /> ${text}"

